
import java.io.*;

/* We have a Class that represents a Duck with flying, swimming and quacking behaviors.
Not all ducks have similar quacking sounds and flying types.
We're gonna make a Superclass to allow istantiating duck objects
---------------------------------*/
public abstract class Duck {

  // Instance variables to handle polimorphic associations with flying and quacking behaviors
  FlyBehavior flyBehavior;
  QuackBehavior quackBehavior;
  
  public Duck() {}
  
  // Every sub-class has to implement its own display() method
  public abstract void display();
  
  // Delegate to the object referenced by flyBehavior
  public void performFly() {
    flyBehavior.fly();
  }
  // Delegate to the object referenced by quackBehavior
  public void performQuack() {
    quackBehavior.quack();
  }
  
  // These allow dynamic injection of behaviors at runtime
  public void setFlyBehavior(FlyBehavior fb) {
    flyBehavior = fb;
  }
  public void setQuackBehavior(QuackBehavior qb) {
    quackBehavior = qb;
  }
  
  // They all swim the same way
  public void swim() {
    System.out.println("All ducks float, even decoys!");
  }

} 
