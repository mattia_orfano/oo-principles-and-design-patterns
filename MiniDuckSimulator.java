
public class MiniDuckSimulator {
  public static void main(String[] args) {
    Duck mallard = new MullardDuck();
    mallard.performQuack();
    mallard.performFly();
    
    // Now, change its behavior!
    mallard.setFlyBehavior(new FlyRocketPowered());
    mallard.performFly();	// Our Mallard duck has now the ability to fly with a rocket
  }
}
