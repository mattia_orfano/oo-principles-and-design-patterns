
import java.io.*;

public class MullardDuck extends Duck {

  public MullardDuck() {
    quackBehavior = new Quack();
    flyBehavior = new FlyWithWings();
  }
  
  public void display() {
    System.out.println("I'm a real Mallard Duck");
  }
  
} 
